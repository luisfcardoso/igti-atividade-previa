const assert = require('assert');
const func = require('../src/say_hello');

describe('sayHello test', () => {
    it('should return greet as the example', () => {
        assert.equal(func.sayHello('test'), 'Hello test!');
    });
})