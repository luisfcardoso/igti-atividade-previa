#! /usr/bin/env node

var express = require('express');
var app = express();

//traffic of / to public folder
app.use('/', express.static('public'));

app.listen(process.env.PORT || 3000, function (){
    console.log('Running on port 3000!');
});